namespace Memo.Game.Models
{
    public enum GameState
    {
        Initializing, Intro, CardsSelection, Matched, NotMatched, Win, Lose
    }
}