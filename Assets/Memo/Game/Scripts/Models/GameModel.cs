using System.Collections.Generic;
using Memo.Card.Models;
using UniRx;

namespace Memo.Game.Models
{
    public class GameModel
    {
        public ReactiveProperty<GameState> State { get; } = new ReactiveProperty<GameState>(GameState.Initializing);

        public List<CardModel> SelectedCards { get; } = new List<CardModel>(2);

        public void Reset()
        {
            SelectedCards.Clear();
        }
    }
}