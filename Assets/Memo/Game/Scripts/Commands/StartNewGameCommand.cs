using Memo.Common.Commands;
using Memo.Common.Events;
using Memo.Game.Events;
using Memo.Game.Models;
using Memo.GameBoard.Events;

namespace Memo.Game.Commands
{
    public class StartNewGameCommand : BaseEventCommand<StartNewGameEvent>
    {
        private readonly IEventProcessor _eventProcessor;
        private readonly GameModel _gameModel;

        public StartNewGameCommand(IEventProcessor eventProcessor, GameModel gameModel)
        {
            _eventProcessor = eventProcessor;
            _gameModel = gameModel;
        }

        protected override void ExecuteEventCommand(StartNewGameEvent commandEvent)
        {
            if (_gameModel.State.Value == GameState.Intro)
            {
                return;
            }

            _gameModel.Reset();

            _eventProcessor.Process(CreateCardsEvent.Instance);

            _gameModel.State.Value = GameState.Intro;
        }
    }
}