using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.Game.Models;

namespace Memo.Game.Commands
{
    public class LoseGameCommand : BaseEventCommand<LoseGameEvent>
    {
        private readonly GameModel _gameModel;

        public LoseGameCommand(GameModel gameModel)
        {
            _gameModel = gameModel;
        }

        protected override void ExecuteEventCommand(LoseGameEvent commandEvent)
        {
            _gameModel.State.Value = GameState.Lose;
        }
    }
}