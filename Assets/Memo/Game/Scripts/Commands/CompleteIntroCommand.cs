using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.Game.Models;

namespace Memo.Game.Commands
{
    public class CompleteIntroCommand : BaseEventCommand<CompleteIntroEvent>
    {
        private readonly GameModel _gameModel;

        public CompleteIntroCommand(GameModel gameModel)
        {
            _gameModel = gameModel;
        }

        protected override void ExecuteEventCommand(CompleteIntroEvent commandEvent)
        {
            if (_gameModel.State.Value != GameState.Intro)
            {
                return;
            }

            _gameModel.State.Value = GameState.CardsSelection;
        }
    }
}