using System.Linq;
using Memo.Card.Events;
using Memo.Card.Models;
using Memo.Common.Commands;
using Memo.Common.Events;
using Memo.Game.Events;
using Memo.Game.Models;
using Memo.GameBoard.Events;
using Memo.GameBoard.Models;

namespace Memo.Game.Commands
{
    public class ClearSelectionCommand : BaseEventCommand<ClearSelectionEvent>
    {
        private readonly GameModel _gameModel;
        private readonly GameBoardModel _gameBoardModel;
        private readonly IEventProcessor _eventProcessor;

        public ClearSelectionCommand(GameModel gameModel, IEventProcessor eventProcessor, GameBoardModel gameBoardModel)
        {
            _gameModel = gameModel;
            _eventProcessor = eventProcessor;
            _gameBoardModel = gameBoardModel;
        }

        protected override void ExecuteEventCommand(ClearSelectionEvent commandEvent)
        {
            CheckMatching();

            CheckNotMatching();
        }

        private void CheckMatching()
        {
            if (_gameModel.State.Value != GameState.Matched)
            {
                return;
            }

            var selectedCards = _gameModel.SelectedCards;
            selectedCards.ForEach(cardModel => _eventProcessor.Process(CollectCardEvent.Create(cardModel)));
            selectedCards.Clear();

            if (_gameBoardModel.Cards.Any(model => model.State.Value != CardState.Collected))
            {
                _gameModel.State.Value = GameState.CardsSelection;    
            }
            else
            {
                _eventProcessor.Process(WinGameEvent.Instance);
            }
        }

        private void CheckNotMatching()
        {
            if (_gameModel.State.Value != GameState.NotMatched)
            {
                return;
            }

            var selectedCards = _gameModel.SelectedCards;
            _eventProcessor.Process(FlipCardsOverEvent.Instance);
            selectedCards.Clear();

            _gameModel.State.Value = GameState.CardsSelection;
        }
    }
}