using Memo.Card.Events;
using Memo.Common.Commands;
using Memo.Common.Events;
using Memo.Game.Events;
using Memo.Game.Models;

namespace Memo.Game.Commands
{
    public class AddSelectedCardCommand : BaseEventCommand<CardSelectedEvent>
    {
        private readonly IEventProcessor _eventProcessor;
        private readonly GameModel _gameModel;

        public AddSelectedCardCommand(IEventProcessor eventProcessor, GameModel gameModel)
        {
            _eventProcessor = eventProcessor;
            _gameModel = gameModel;
        }

        protected override void ExecuteEventCommand(CardSelectedEvent commandEvent)
        {
            if (_gameModel.State.Value != GameState.CardsSelection)
            {
                return;
            }

            var cardModel = commandEvent.CardModel;

            if (_gameModel.SelectedCards.Contains(cardModel))
            {
                return;
            }

            _eventProcessor.Process(FlipCardOnEvent.Create(cardModel));
            _gameModel.SelectedCards.Add(cardModel);

            if (_gameModel.SelectedCards.Count >= 2)
            {
                _eventProcessor.Process(CardsSelectedEvent.Instance);
            }
        }
    }
}