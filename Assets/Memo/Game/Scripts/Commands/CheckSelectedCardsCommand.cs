using System.Collections.Generic;
using System.Linq;
using Memo.Card.Models;
using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.Game.Models;

namespace Memo.Game.Commands
{
    public class CheckSelectedCardsCommand : BaseEventCommand<CardsSelectedEvent>
    {
        private readonly GameModel _gameModel;

        public CheckSelectedCardsCommand(GameModel gameModel)
        {
            _gameModel = gameModel;
        }

        protected override void ExecuteEventCommand(CardsSelectedEvent commandEvent)
        {
            if (_gameModel.State.Value != GameState.CardsSelection)
            {
                return;
            }

            var selectedCards = _gameModel.SelectedCards;

            if (CheckMatching(selectedCards))
            {
                _gameModel.State.Value = GameState.Matched;
            }
            else
            {
                _gameModel.State.Value = GameState.NotMatched;
            }
        }

        private bool CheckMatching(List<CardModel> selectedCards)
        {
            var firstSelectedCardName = selectedCards[0].Name;

            return selectedCards.All(cardModel => cardModel.Name == firstSelectedCardName);
        }
    }
}