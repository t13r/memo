using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.Game.Models;

namespace Memo.Game.Commands
{
    public class WinGameCommand : BaseEventCommand<WinGameEvent>
    {
        private readonly GameModel _gameModel;

        public WinGameCommand(GameModel gameModel)
        {
            _gameModel = gameModel;
        }

        protected override void ExecuteEventCommand(WinGameEvent commandEvent)
        {
            _gameModel.State.Value = GameState.Win;
        }
    }
}