using Memo.Common.Events;
using Memo.Game.Events;
using Memo.Game.Models;
using Memo.GameBoard.Events;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Memo.Game.Mediators
{
    public class GameMediator : MonoBehaviour
    {
        [SerializeField] private UnityEvent<string> OnEmitState;
        
        private IEventProcessor _eventProcessor;
        private GameModel _gameModel;

        [Inject]
        public void Construct(GameModel gameModel, IEventProcessor eventProcessor)
        {
            _gameModel = gameModel;
            _eventProcessor = eventProcessor;

            Init();
        }

        public void RestartGame()
        {
            _eventProcessor.Process(StartNewGameEvent.Instance);
        }

        public void FlipCardsOn()
        {
            _eventProcessor.Process(FlipCardsOnEvent.Instance);
        }

        public void FlipCardsOver()
        {
            _eventProcessor.Process(FlipCardsOverEvent.Instance);
        }

        public void OnIntroCompleted()
        {
            _eventProcessor.Process(CompleteIntroEvent.Instance);
        }

        public void OnMatchingVisualizationCompleted()
        {
            _eventProcessor.Process(ClearSelectionEvent.Instance);
        }

        public void OnNotMatchingVisualizationCompleted()
        {
            _eventProcessor.Process(ClearSelectionEvent.Instance);
        }

        private void Init()
        {
            _gameModel.State
                .AsObservable()
                .Subscribe(state => OnEmitState.Invoke(state.ToString()))
                .AddTo(this);            
        }

        private void Start()
        {
            _eventProcessor.Process(StartNewGameEvent.Instance);
        }
    }
}