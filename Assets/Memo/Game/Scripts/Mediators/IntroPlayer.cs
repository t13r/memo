using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Memo.Game.Mediators
{
    public class IntroPlayer : MonoBehaviour
    {
        [SerializeField] private float _delayStartTime = 1f;
        [SerializeField] private float _cardsShowTime = 5f;

        [SerializeField] private UnityEvent OnFlipCardsOver;
        [SerializeField] private UnityEvent OnFlipCardsOn;
        [SerializeField] private UnityEvent OnCompleteIntro;

        public void PlayIntro()
        {
            StartCoroutine(PlayingIntro());
        }

        private IEnumerator PlayingIntro()
        {
            OnFlipCardsOver.Invoke();

            yield return new WaitForSeconds(_delayStartTime);

            OnFlipCardsOn.Invoke();

            yield return new WaitForSeconds(_cardsShowTime);

            OnFlipCardsOver.Invoke();
            OnCompleteIntro.Invoke();
        }
    }
}