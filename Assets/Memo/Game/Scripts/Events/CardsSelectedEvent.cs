using Memo.Common.Events;

namespace Memo.Game.Events
{
    public class CardsSelectedEvent : IEvent
    {
        public static CardsSelectedEvent Instance { get; } = new CardsSelectedEvent();

        private CardsSelectedEvent()
        {
            //empty
        }
    }
}