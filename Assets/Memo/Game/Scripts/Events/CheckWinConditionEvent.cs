using Memo.Common.Events;

namespace Memo.Game.Events
{
    public class CheckWinConditionEvent : IEvent
    {
        public static CheckWinConditionEvent Instance { get; } = new CheckWinConditionEvent();

        private CheckWinConditionEvent()
        {
            //empty
        }
    }
}