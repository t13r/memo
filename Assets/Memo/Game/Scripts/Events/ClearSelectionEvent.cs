using Memo.Common.Events;

namespace Memo.Game.Events
{
    public class ClearSelectionEvent : IEvent
    {
        public static ClearSelectionEvent Instance { get; } = new ClearSelectionEvent();

        private ClearSelectionEvent()
        {
            //empty
        }
    }
}