using Memo.Common.Events;

namespace Memo.Game.Events
{
    public class WinGameEvent : IEvent
    {
        public static WinGameEvent Instance { get; } = new WinGameEvent();

        private WinGameEvent()
        {
            //empty
        }
    }
}