using Memo.Common.Events;

namespace Memo.Game.Events
{
    public class StartNewGameEvent : IEvent
    {
        public static StartNewGameEvent Instance { get; } = new StartNewGameEvent();

        private StartNewGameEvent()
        {
            //empty
        }
    }
}