using Memo.Common.Events;

namespace Memo.Game.Events
{
    public class LoseGameEvent : IEvent
    {
        public static LoseGameEvent Instance { get; } = new LoseGameEvent();

        private LoseGameEvent()
        {
            //empty
        }
    }
}