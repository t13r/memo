using Memo.Common.Events;

namespace Memo.Game.Events
{
    public class CompleteIntroEvent : IEvent
    {
        public static CompleteIntroEvent Instance { get; } = new CompleteIntroEvent();

        private CompleteIntroEvent()
        {
            //empty
        }
    }
}