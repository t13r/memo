using Memo.Game.Commands;
using Memo.Game.Models;
using Memo.LoseConditions;
using Memo.LoseConditions.TimeLimit;
using Memo.LoseConditions.TimeLimit.Models;
using Memo.LoseConditions.TimeLimit.Settings;
using UnityEngine;
using Zenject;

namespace Memo.Game
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private TimeLimitLoseConditionSettings _timeLimitLoseConditionSettings;
        
        public override void InstallBindings()
        {
            InstallInstances();
            InstallCommands();
            InstallLoseConditions();
        }

        private void InstallLoseConditions()
        {
            Container.BindInterfacesAndSelfTo<LoseConditionsChecker>().AsSingle();

            if (_timeLimitLoseConditionSettings != null)
            {
                TimeLimitInstaller.Install(Container, _timeLimitLoseConditionSettings);
            }
        }

        private void InstallInstances()
        {
            var gameModel = new GameModel();

            Container.BindInstances(gameModel);
        }

        private void InstallCommands()
        {
            Container.BindInterfacesAndSelfTo<StartNewGameCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<CheckSelectedCardsCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<AddSelectedCardCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<ClearSelectionCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<CompleteIntroCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<LoseGameCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<WinGameCommand>().AsSingle();
        }
    }
}