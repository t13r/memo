using System.Collections.Generic;
using Memo.Card.Models;

namespace Memo.GameBoard.Models
{
    public class GameBoardModel
    {
        public List<CardModel> Cards { get; } = new List<CardModel>();
    }
}