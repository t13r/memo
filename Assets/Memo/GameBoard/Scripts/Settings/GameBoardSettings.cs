using System.Collections.Generic;
using Memo.Card.Models;
using Memo.LoseConditions.TimeLimit.Settings;
using UnityEngine;

namespace Memo.GameBoard.Settings
{
    [CreateAssetMenu(fileName = "GameBoardSettings", menuName = "Memo/GameBoard/GameBoardSettings")]
    public class GameBoardSettings : ScriptableObject
    {
        [SerializeField] private CardsSetSettings _cardsSetSettings;
        [SerializeField] private List<string> _cardNames;

        public CardsSetSettings CardsSetSettings => _cardsSetSettings;

        public List<string> CardNames => _cardNames;
    }
}