using System;
using System.Linq;
using Memo.Card.Mediators;
using Memo.Card.Models;
using Memo.Common.Commands;
using Memo.GameBoard.Events;
using Memo.GameBoard.Models;
using Memo.GameBoard.Settings;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Memo.GameBoard.Commands
{
    public class CreateCardsCommand : BaseEventCommand<CreateCardsEvent>
    {
        private readonly GameBoardModel _gameBoardModel;
        private readonly GameBoardSettings _gameBoardSettings;
        private readonly CardMediator.Factory _cardFactory;
        
        private readonly Transform _cardParent;

        public CreateCardsCommand(GameBoardModel gameBoardModel, GameBoardSettings gameBoardSettings, 
            CardMediator.Factory cardFactory, [Inject(Id = "CardParentTransform")] Transform cardParent)
        {
            _gameBoardModel = gameBoardModel;
            _gameBoardSettings = gameBoardSettings;
            _cardFactory = cardFactory;
            _cardParent = cardParent;
        }

        protected override void ExecuteEventCommand(CreateCardsEvent commandEvent)
        {
            DestroyOldCards();
            _gameBoardModel.Cards.Clear();

            _gameBoardSettings.CardNames.ForEach(CreateCardModel);

            _gameBoardModel.Cards.OrderBy((cardModel) => Guid.NewGuid())
                .ToList()
                .ForEach(CreateCard);
        }

        private void DestroyOldCards()
        {
            foreach (Transform child in _cardParent)
            {
                Object.Destroy(child.gameObject);
            }
        }

        private void CreateCardModel(string cardName)
        {
            var cardsSettings = _gameBoardSettings.CardsSetSettings.Cards;
            var cardSettings = cardsSettings.FirstOrDefault(card => card.Name == cardName);
            
            if (cardSettings == null)
            {
                Debug.LogWarning($"Missing '{cardName}' card in settings");

                return;
            }

            var firstCardModel = new CardModel(cardSettings.Name, cardSettings.Sprite);
            _gameBoardModel.Cards.Add(firstCardModel);

            var secondCardModel = new CardModel(cardSettings.Name, cardSettings.Sprite);
            _gameBoardModel.Cards.Add(secondCardModel);
        }

        private void CreateCard(CardModel cardModel)
        {
            var cardMediator = _cardFactory.Create(cardModel);

            cardMediator.transform.SetParent(_cardParent, false);
        }
    }
}