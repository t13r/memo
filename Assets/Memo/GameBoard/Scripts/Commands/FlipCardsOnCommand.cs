using Memo.Card.Events;
using Memo.Card.Models;
using Memo.Common.Commands;
using Memo.Common.Events;
using Memo.GameBoard.Events;
using Memo.GameBoard.Models;

namespace Memo.GameBoard.Commands
{
    public class FlipCardsOnCommand : BaseEventCommand<FlipCardsOnEvent>
    {
        private readonly GameBoardModel _gameBoardModel;
        private readonly IEventProcessor _eventProcessor;

        public FlipCardsOnCommand(GameBoardModel gameBoardModel, IEventProcessor eventProcessor)
        {
            _gameBoardModel = gameBoardModel;
            _eventProcessor = eventProcessor;
        }

        protected override void ExecuteEventCommand(FlipCardsOnEvent commandEvent)
        {
            _gameBoardModel.Cards.ForEach(FlipCardOn);
        }

        private void FlipCardOn(CardModel cardModel)
        {
            _eventProcessor.Process(FlipCardOnEvent.Create(cardModel));
        }
    }
}