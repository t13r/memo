using Memo.Card.Events;
using Memo.Card.Models;
using Memo.Common.Commands;
using Memo.Common.Events;
using Memo.GameBoard.Events;
using Memo.GameBoard.Models;

namespace Memo.GameBoard.Commands
{
    public class FlipCardsOverCommand : BaseEventCommand<FlipCardsOverEvent>
    {
        private readonly GameBoardModel _gameBoardModel;
        private readonly IEventProcessor _eventProcessor;

        public FlipCardsOverCommand(GameBoardModel gameBoardModel, IEventProcessor eventProcessor)
        {
            _gameBoardModel = gameBoardModel;
            _eventProcessor = eventProcessor;
        }

        protected override void ExecuteEventCommand(FlipCardsOverEvent commandEvent)
        {
            _gameBoardModel.Cards.ForEach(FlipCardOver);
        }

        private void FlipCardOver(CardModel cardModel)
        {
            _eventProcessor.Process(FlipCardOverEvent.Create(cardModel));
        }
    }
}