using Memo.Common.Events;

namespace Memo.GameBoard.Events
{
    public class FlipCardsOnEvent : IEvent
    {
        public static FlipCardsOnEvent Instance { get; } = new FlipCardsOnEvent();

        private FlipCardsOnEvent()
        {
            //empty
        }
    }
}