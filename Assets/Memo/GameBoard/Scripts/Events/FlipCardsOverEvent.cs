using Memo.Common.Events;

namespace Memo.GameBoard.Events
{
    public class FlipCardsOverEvent : IEvent
    {
        public static FlipCardsOverEvent Instance { get; } = new FlipCardsOverEvent();

        private FlipCardsOverEvent()
        {
            //empty
        }
    }
}