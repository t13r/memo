using Memo.Common.Events;

namespace Memo.GameBoard.Events
{
    public class CreateCardsEvent : IEvent
    {
        public static CreateCardsEvent Instance { get; } = new CreateCardsEvent();

        private CreateCardsEvent()
        {
            //empty
        }
    }
}