using Memo.GameBoard.Commands;
using Memo.GameBoard.Models;
using Memo.GameBoard.Settings;
using UnityEngine;
using Zenject;

namespace Memo.GameBoard
{
    public class GameBoardInstaller : MonoInstaller
    {
        [SerializeField] private GameBoardSettings _gameBoardSettings;
        [SerializeField] private Transform _cardParent;

        public override void InstallBindings()
        {
            InstallCommands();
            InstallInstances();
        }

        private void InstallCommands()
        {
            Container.BindInterfacesAndSelfTo<CreateCardsCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<FlipCardsOnCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<FlipCardsOverCommand>().AsSingle();
        }

        private void InstallInstances()
        {
            var gameBoardModel = new GameBoardModel();

            Container.BindInstances(_gameBoardSettings, gameBoardModel);

            Container.BindInstance(_cardParent).WithId("CardParentTransform");
        }
    }
}