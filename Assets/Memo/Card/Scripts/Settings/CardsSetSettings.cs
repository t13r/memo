using System;
using System.Collections.Generic;
using UnityEngine;

namespace Memo.Card.Models
{
    [CreateAssetMenu(fileName = "CardsSetSettings", menuName = "Memo/Card/CardsSetSettings")]
    public class CardsSetSettings : ScriptableObject
    {
        [SerializeField] private List<Card> _cards;

        public List<Card> Cards => _cards;

        [Serializable]
        public class Card
        {
            [SerializeField] private string _name;
            [SerializeField] private Sprite _sprite;

            public string Name => _name;

            public Sprite Sprite => _sprite;
        }
    }
}