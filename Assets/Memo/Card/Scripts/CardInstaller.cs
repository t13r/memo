using Memo.Card.Commands;
using Memo.Card.Mediators;
using Memo.Card.Models;
using UnityEngine;
using Zenject;

namespace Memo.Card
{
    public class CardInstaller : MonoInstaller
    {
        [SerializeField] private CardMediator _cardPrefab;

        public override void InstallBindings()
        {
            InstallCommands();
            InstallFactories();
        }

        private void InstallFactories()
        {
            Container.BindFactory<CardModel, CardMediator, CardMediator.Factory>().FromComponentInNewPrefab(_cardPrefab).AsSingle();
        }

        private void InstallCommands()
        {
            Container.BindInterfacesAndSelfTo<FlipCardOnCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<FlipCardOverCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<CollectCardCommand>().AsSingle();
        }
    }
}