namespace Memo.Card.Models
{
    public enum CardState
    {
        On, Over, Collected
    }
}