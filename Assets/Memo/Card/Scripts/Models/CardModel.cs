﻿using UniRx;
using UnityEngine;

namespace Memo.Card.Models
{
    public class CardModel
    {
        public string Name { get; }
        public Sprite Sprite { get; }

        public ReactiveProperty<CardState> State { get; } = new ReactiveProperty<CardState>(CardState.Over);

        public CardModel(string name, Sprite sprite)
        {
            Name = name;
            Sprite = sprite;
        }
    }
}