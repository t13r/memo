using Memo.Card.Models;

namespace Memo.Card.Events
{
    public class CollectCardEvent : BaseCardEvent
    {
        private CollectCardEvent(CardModel cardModel) : base(cardModel)
        {
            //empty
        }

        public static CollectCardEvent Create(CardModel cardModel)
        {
            return new CollectCardEvent(cardModel);
        }
    }
}