using Memo.Card.Models;

namespace Memo.Card.Events
{
    public class FlipCardOnEvent : BaseCardEvent
    {
        private FlipCardOnEvent(CardModel cardModel) : base(cardModel)
        {
            //empty
        }

        public static FlipCardOnEvent Create(CardModel cardModel)
        {
            return new FlipCardOnEvent(cardModel);
        }
    }
}