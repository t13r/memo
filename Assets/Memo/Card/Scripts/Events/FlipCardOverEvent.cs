using Memo.Card.Models;

namespace Memo.Card.Events
{
    public class FlipCardOverEvent : BaseCardEvent
    {
        private FlipCardOverEvent(CardModel cardModel) : base(cardModel)
        {
            //empty
        }

        public static FlipCardOverEvent Create(CardModel cardModel)
        {
            return new FlipCardOverEvent(cardModel);
        }
    }
}