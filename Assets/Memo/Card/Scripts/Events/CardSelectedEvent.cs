using Memo.Card.Models;

namespace Memo.Card.Events
{
    public class CardSelectedEvent : BaseCardEvent
    {
        private CardSelectedEvent(CardModel cardModel) : base(cardModel)
        {
            //empty
        }

        public static CardSelectedEvent Create(CardModel cardModel)
        {
            return new CardSelectedEvent(cardModel);
        }
    }
}