using Memo.Card.Models;
using Memo.Common.Events;

namespace Memo.Card.Events
{
    public abstract class BaseCardEvent : IEvent
    {
        public CardModel CardModel { get; }

        protected BaseCardEvent(CardModel cardModel)
        {
            CardModel = cardModel;
        }
    }
}