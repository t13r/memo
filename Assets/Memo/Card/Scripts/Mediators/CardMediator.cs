using Memo.Card.Events;
using Memo.Card.Models;
using Memo.Common.Events;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Memo.Card.Mediators
{
    public class CardMediator : MonoBehaviour
    {
        [SerializeField] private UnityEvent<string> OnEmitState;
        [SerializeField] private UnityEvent<Sprite> OnEmitSprite;

        private CardModel _cardModel;
        private IEventProcessor _eventProcessor;

        [Inject]
        public void Construct(CardModel cardModel, IEventProcessor eventProcessor)
        {
            _cardModel = cardModel;
            _eventProcessor = eventProcessor;

            Init();
        }

        public void HandleCardSelected()
        {
            _eventProcessor.Process(CardSelectedEvent.Create(_cardModel));
        }

        private void Init()
        {
            OnEmitSprite.Invoke(_cardModel.Sprite);

            _cardModel.State
                .AsObservable()
                .Subscribe(state => OnEmitState.Invoke(state.ToString()))
                .AddTo(this);
        }

        public class Factory : PlaceholderFactory<CardModel, CardMediator>
        {
        }
    }
}