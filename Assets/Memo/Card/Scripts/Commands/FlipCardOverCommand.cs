using Memo.Card.Events;
using Memo.Card.Models;
using Memo.Common.Commands;

namespace Memo.Card.Commands
{
    public class FlipCardOverCommand : BaseEventCommand<FlipCardOverEvent>
    {
        protected override void ExecuteEventCommand(FlipCardOverEvent commandEvent)
        {
            var cardState = commandEvent.CardModel.State;

            if (cardState.Value != CardState.On)
            {
                return;
            }

            cardState.Value = CardState.Over;
        }
    }
}