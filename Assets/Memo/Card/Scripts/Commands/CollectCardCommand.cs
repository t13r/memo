using Memo.Card.Events;
using Memo.Card.Models;
using Memo.Common.Commands;

namespace Memo.Card.Commands
{
    public class CollectCardCommand : BaseEventCommand<CollectCardEvent>
    {
        protected override void ExecuteEventCommand(CollectCardEvent commandEvent)
        {
            var cardState = commandEvent.CardModel.State;

            if (cardState.Value != CardState.On)
            {
                return;
            }

            cardState.Value = CardState.Collected;
        }
    }
}