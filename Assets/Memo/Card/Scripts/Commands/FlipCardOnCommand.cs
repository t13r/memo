using Memo.Card.Events;
using Memo.Card.Models;
using Memo.Common.Commands;

namespace Memo.Card.Commands
{
    public class FlipCardOnCommand : BaseEventCommand<FlipCardOnEvent>
    {
        protected override void ExecuteEventCommand(FlipCardOnEvent commandEvent)
        {
            var cardState = commandEvent.CardModel.State;

            if (cardState.Value != CardState.Over)
            {
                return;
            }

            cardState.Value = CardState.On;
        }
    }
}