using System.Collections.Generic;
using System.Linq;
using Memo.Common.Events;
using Memo.Game.Events;
using Zenject;

namespace Memo.LoseConditions
{
    public class LoseConditionsChecker : IFixedTickable
    {
        private readonly List<ILoseCondition> _loseConditions;
        private readonly IEventProcessor _eventProcessor;

        public LoseConditionsChecker(List<ILoseCondition> loseConditions, IEventProcessor eventProcessor)
        {
            _loseConditions = loseConditions;
            _eventProcessor = eventProcessor;
        }

        public void FixedTick()
        {
            if (_loseConditions.Any(condition => condition.Check()))
            {
                _eventProcessor.Process(LoseGameEvent.Instance);
            }
        }
    }
}