using System;
using UniRx;
using UnityEngine;

namespace Memo.LoseConditions.TimeLimit.Models
{
    public class TimeLimitModel
    {
        public ReactiveProperty<TimeLimitState> State { get; } =
            new ReactiveProperty<TimeLimitState>(TimeLimitState.Idle);

        public float StartTime { get; private set; }

        public float TimeLimit { get; }

        public int TimeLeft
        {
            get
            {
                if (State.Value == TimeLimitState.Idle)
                {
                    return 0;
                }

                var timeLeft = Convert.ToInt32(StartTime + TimeLimit - Time.time);

                return Mathf.Max(timeLeft, 0);
            }
        }

        public TimeLimitModel(float timeLimit)
        {
            TimeLimit = timeLimit;
        }

        public void StartTimer()
        {
            StartTime = Time.time;

            State.Value = TimeLimitState.Run;
        }

        public void StopTimer()
        {
            State.Value = TimeLimitState.Idle;

            StartTime = 0;
        }
    }
}