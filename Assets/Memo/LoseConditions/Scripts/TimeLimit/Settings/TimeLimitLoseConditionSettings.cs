using UnityEngine;

namespace Memo.LoseConditions.TimeLimit.Settings
{
    [CreateAssetMenu(fileName = "TimeLimit", menuName = "Memo/LoseConditions/TimeLimit")]
    public class TimeLimitLoseConditionSettings : ScriptableObject
    {
        [SerializeField] private float _timeLimit;

        public float TimeLimit => _timeLimit;
    }
}