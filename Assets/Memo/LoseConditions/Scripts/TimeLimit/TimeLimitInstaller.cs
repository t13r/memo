using Memo.LoseConditions.TimeLimit.Commands;
using Memo.LoseConditions.TimeLimit.Models;
using Memo.LoseConditions.TimeLimit.Settings;
using Zenject;

namespace Memo.LoseConditions.TimeLimit
{
    public class TimeLimitInstaller : Installer<TimeLimitLoseConditionSettings, TimeLimitInstaller>
    {
        private readonly TimeLimitLoseConditionSettings _timeLimitLoseConditionSettings;

        public TimeLimitInstaller(TimeLimitLoseConditionSettings timeLimitLoseConditionSettings)
        {
            _timeLimitLoseConditionSettings = timeLimitLoseConditionSettings;
        }

        public override void InstallBindings()
        {
            InstallCommands();
            InstallLoseCondition();
        }

        private void InstallCommands()
        {
            Container.BindInterfacesAndSelfTo<StartGameTimerCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<StopGameTimerOnLoseCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<StopGameTimerOnWinCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<StopGameTimerOnRestartCommand>().AsSingle();
        }

        private void InstallLoseCondition()
        {
            var timeLimitModel = new TimeLimitModel(_timeLimitLoseConditionSettings.TimeLimit);

            Container.BindInstance(timeLimitModel);
            Container.Bind<ILoseCondition>().To<TimeLimitLoseCondition>().AsSingle();
        }
    }
}