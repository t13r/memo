using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.LoseConditions.TimeLimit.Models;

namespace Memo.LoseConditions.TimeLimit.Commands
{
    public class StopGameTimerOnLoseCommand : BaseEventCommand<LoseGameEvent>
    {
        private readonly TimeLimitModel _conditionModel;

        public StopGameTimerOnLoseCommand(TimeLimitModel conditionModel)
        {
            _conditionModel = conditionModel;
        }

        protected override void ExecuteEventCommand(LoseGameEvent commandEvent)
        {
            _conditionModel.StopTimer();
        }
    }
}