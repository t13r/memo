using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.LoseConditions.TimeLimit.Models;

namespace Memo.LoseConditions.TimeLimit.Commands
{
    public class StopGameTimerOnWinCommand : BaseEventCommand<WinGameEvent>
    {
        private readonly TimeLimitModel _conditionModel;

        public StopGameTimerOnWinCommand(TimeLimitModel conditionModel)
        {
            _conditionModel = conditionModel;
        }

        protected override void ExecuteEventCommand(WinGameEvent commandEvent)
        {
            _conditionModel.StopTimer();
        }
    }
}