using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.LoseConditions.TimeLimit.Models;

namespace Memo.LoseConditions.TimeLimit.Commands
{
    public class StopGameTimerOnRestartCommand : BaseEventCommand<StartNewGameEvent>
    {
        private readonly TimeLimitModel _conditionModel;

        public StopGameTimerOnRestartCommand(TimeLimitModel conditionModel)
        {
            _conditionModel = conditionModel;
        }

        protected override void ExecuteEventCommand(StartNewGameEvent commandEvent)
        {
            _conditionModel.StopTimer();
        }
    }
}