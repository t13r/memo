using Memo.Common.Commands;
using Memo.Game.Events;
using Memo.LoseConditions.TimeLimit.Models;

namespace Memo.LoseConditions.TimeLimit.Commands
{
    public class StartGameTimerCommand : BaseEventCommand<CompleteIntroEvent>
    {
        private readonly TimeLimitModel _conditionModel;

        public StartGameTimerCommand(TimeLimitModel conditionModel)
        {
            _conditionModel = conditionModel;
        }

        protected override void ExecuteEventCommand(CompleteIntroEvent commandEvent)
        {
            _conditionModel.StartTimer();
        }
    }
}