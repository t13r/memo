using Memo.LoseConditions.TimeLimit.Models;
using UnityEngine;

namespace Memo.LoseConditions.TimeLimit
{
    public class TimeLimitLoseCondition : ILoseCondition
    {
        private readonly TimeLimitModel _timeLimitModel;

        public TimeLimitLoseCondition(TimeLimitModel timeLimitModel)
        {
            _timeLimitModel = timeLimitModel;
        }

        public bool Check()
        {
            if (_timeLimitModel.State.Value != TimeLimitState.Run)
            {
                return false;
            }

            return _timeLimitModel.StartTime + _timeLimitModel.TimeLimit <= Time.time;
        }
    }
}