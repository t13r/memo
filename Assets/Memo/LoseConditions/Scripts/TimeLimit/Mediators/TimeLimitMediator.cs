using Memo.LoseConditions.TimeLimit.Models;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Memo.LoseConditions.TimeLimit.Mediators
{
    public class TimeLimitMediator : MonoBehaviour
    {
        [SerializeField] private UnityEvent<int> OnEmitTimeLeft;
        [SerializeField] private UnityEvent<string> OnEmitState;

        private TimeLimitModel _timeLimitModel;

        [Inject]
        public void Construct(TimeLimitModel timeLimitModel)
        {
            _timeLimitModel = timeLimitModel;

            Init();
        }

        private void Init()
        {
            _timeLimitModel.State.AsObservable()
                .Subscribe(state => OnEmitState.Invoke(state.ToString()))
                .AddTo(this);
        }

        private void FixedUpdate()
        {
            OnEmitTimeLeft.Invoke(_timeLimitModel.TimeLeft);
        }
    }
}