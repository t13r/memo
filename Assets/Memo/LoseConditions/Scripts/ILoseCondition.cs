namespace Memo.LoseConditions
{
    public interface ILoseCondition
    {
        bool Check();
    }
}