using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Memo.Common.Components
{
    public class DelayedCallComponent : MonoBehaviour
    {
        [SerializeField] private UnityEvent _event;

        public void Call(float time)
        {
            StartCoroutine(CallAfterDelay(time));
        }

        private IEnumerator CallAfterDelay(float time)
        {
            yield return new WaitForSeconds(time);

            _event.Invoke();
        }
    }
}