﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Memo.Common.Components
{
    public class StateComponent : MonoBehaviour
    {
        [SerializeField] private string _initialState;
        [SerializeField] private List<State> _states;

        private State _currentState;

        private void Awake()
        {
            if (string.IsNullOrEmpty(_initialState))
            {
                return;
            }

            _currentState = GetState(_initialState);
        }

        public void SetState(string stateName)
        {
            var newState = GetState(stateName);

            if (newState == null)
            {
                Debug.LogWarning($"State with name {stateName} is not found", this);

                return;
            }

            _currentState?.OnExit();
            _currentState = newState;
            _currentState.OnEnter();
        }

        private State GetState(string stateName)
        {
            return _states.FirstOrDefault(state => state.Name == stateName);
        }

        [Serializable]
        public class State
        {
            [SerializeField] private string _name;
            [SerializeField] private UnityEvent _onEnter;
            [SerializeField] private UnityEvent _onExit;

            public string Name => _name;

            public void OnEnter()
            {
                _onEnter.Invoke();
            }

            public void OnExit()
            {
                _onExit.Invoke();
            }
        }
    }
}