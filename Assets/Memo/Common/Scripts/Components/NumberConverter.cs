using UnityEngine;
using UnityEngine.Events;

namespace Memo.Common.Components
{
    public class NumberConverter : MonoBehaviour
    {
        [SerializeField] private UnityEvent<string> OnValue;

        public void Convert(int number)
        {
            OnValue.Invoke(number.ToString());
        }
    }
}