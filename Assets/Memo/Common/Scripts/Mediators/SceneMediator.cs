using Memo.Common.Events;
using UnityEngine;
using Zenject;

namespace Memo.Common.Mediators
{
    public class SceneMediator : MonoBehaviour
    {
        private IEventProcessor _eventProcessor;

        [Inject]
        public void Construct(IEventProcessor eventProcessor)
        {
            _eventProcessor = eventProcessor;
        }

        public void ChangeScene(string sceneName)
        {
            _eventProcessor.Process(ChangeSceneEvent.Create(sceneName));
        }
    }
}