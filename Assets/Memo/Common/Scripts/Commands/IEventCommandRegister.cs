using System.Collections.Generic;
using Memo.Common.Events;

namespace Memo.Common.Commands
{
    public interface IEventCommandRegister
    {
        void Register(IEventCommand eventCommand);

        void Unregister(IEventCommand eventCommand);

        List<IEventCommand> GetCommands(IEvent eventCommand);
    }
}