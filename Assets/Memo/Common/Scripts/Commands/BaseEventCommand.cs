using System;
using Memo.Common.Events;
using Zenject;

namespace Memo.Common.Commands
{
    public abstract class BaseEventCommand<T> : IEventCommand, IInitializable, IDisposable 
        where T : IEvent
    {
        [Inject] private IEventCommandRegister _eventCommandRegister;

        public Type EventType => typeof(T);

        protected abstract void ExecuteEventCommand(T commandEvent);

        public void Execute(IEvent commandEvent)
        {
            ExecuteEventCommand((T) commandEvent);
        }

        public void Initialize()
        {
            _eventCommandRegister.Register(this);
        }

        public void Dispose()
        {
            _eventCommandRegister.Unregister(this);
        }
    }
}