using System;
using Memo.Common.Events;

namespace Memo.Common.Commands
{
    public interface IEventCommand
    {
        Type EventType { get; }

        void Execute(IEvent commandEvent);
    }
}