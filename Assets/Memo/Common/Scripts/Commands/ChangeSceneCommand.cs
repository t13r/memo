using Memo.Common.Events;
using UnityEngine.SceneManagement;

namespace Memo.Common.Commands
{
    public class ChangeSceneCommand : BaseEventCommand<ChangeSceneEvent>
    {
        protected override void ExecuteEventCommand(ChangeSceneEvent commandEvent)
        {
            SceneManager.LoadScene(commandEvent.SceneName);
        }
    }
}