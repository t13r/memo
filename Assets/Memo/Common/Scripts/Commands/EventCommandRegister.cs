using System;
using System.Collections.Generic;
using Memo.Common.Events;
using UnityEngine;

namespace Memo.Common.Commands
{
    public class EventCommandRegister : IEventCommandRegister
    {
        private readonly Dictionary<Type, List<IEventCommand>> _eventCommands = new Dictionary<Type, List<IEventCommand>>();
        
        public void Register(IEventCommand eventCommand)
        {
            var eventType = eventCommand.EventType;

            List<IEventCommand> commands;

            if (_eventCommands.ContainsKey(eventType))
            {
                commands = _eventCommands[eventType];
            }
            else
            {
                commands = new List<IEventCommand>();

                _eventCommands.Add(eventType, commands);
            }

            commands.Add(eventCommand);
        }

        public void Unregister(IEventCommand eventCommand)
        {
            var eventType = eventCommand.EventType;

            if (!_eventCommands.ContainsKey(eventType))
            {
                return;
            }

            var commands = _eventCommands[eventType];
            commands.Remove(eventCommand);
        }

        public List<IEventCommand> GetCommands(IEvent eventCommand)
        {
            var eventType = eventCommand.GetType();

            if (_eventCommands.TryGetValue(eventType, out var commands))
            {
                return commands;
            }

            return new List<IEventCommand>();
        }
    }
}