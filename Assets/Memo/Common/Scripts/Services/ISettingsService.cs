namespace Memo.Common.Services
{
    public interface ISettingsService
    {
        void SetFloat(string key, float value);
        void SetInt(string key, int value);
        void SetString(string key, string value);

        float GetFloat(string key);
        int GetInt(string key);
        string GetString(string key);

        void Save();
        void Load();
    }
}