using System.Collections.Generic;
using System.Globalization;

namespace Memo.Common.Services
{
    public class MemorySettingsService : ISettingsService
    {
        private readonly Dictionary<string, string> _settings = new Dictionary<string, string>();

        public void SetFloat(string key, float value)
        {
            _settings[key] = value.ToString(CultureInfo.InvariantCulture);
        }

        public void SetInt(string key, int value)
        {
            _settings[key] = value.ToString();
        }

        public void SetString(string key, string value)
        {
            _settings[key] = value;
        }

        public float GetFloat(string key)
        {
            if (_settings.ContainsKey(key) && float.TryParse(_settings[key], NumberStyles.Float, CultureInfo.InvariantCulture, out var result))
            {
                return result;
            }

            return default;
        }

        public int GetInt(string key)
        {
            if (_settings.ContainsKey(key) && int.TryParse(_settings[key], out var result))
            {
                return result;
            }

            return default;
        }

        public string GetString(string key)
        {
            if (_settings.ContainsKey(key))
            {
                return _settings[key];
            }

            return string.Empty;
        }

        public void Save()
        {
            // empty
        }

        public void Load()
        {
            // empty
        }
    }
}