using System.IO;
using Memo.Common.Commands;
using Memo.Common.Events;
using Memo.Common.Services;
using UnityEngine;
using Zenject;

namespace Memo.Common
{
    [CreateAssetMenu(fileName = "CommonInstaller", menuName = "Memo/CommonInstaller")]
    public class CommonInstaller : ScriptableObjectInstaller<CommonInstaller>
    {
        public override void InstallBindings()
        {
            InstallSettingsService();

            InstallEventProcessor();
            InstallCommands();
        }

        private void InstallSettingsService()
        {
            var settingsFilePath = Path.Combine(Application.dataPath, "settings.json");

            Container.Bind<ISettingsService>().To<FileSettingsService>().AsSingle().WithArguments(settingsFilePath);
            // Container.Bind<ISettingsService>().To<MemorySettingsService>().AsSingle();
            // Container.Bind<ISettingsService>().To<PlayerPrefsSettingsService>().AsSingle();
        }

        private void InstallEventProcessor()
        {
            Container.Bind<IEventCommandRegister>().To<EventCommandRegister>().AsSingle();
            Container.Bind<IEventProcessor>().To<EventProcessor>().AsSingle();
        }

        private void InstallCommands()
        {
            Container.BindInterfacesAndSelfTo<ChangeSceneCommand>().AsSingle();
        }
    }
}