using Memo.Common.Commands;

namespace Memo.Common.Events
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IEventCommandRegister _eventCommandRegister;

        public EventProcessor(IEventCommandRegister eventCommandRegister)
        {
            _eventCommandRegister = eventCommandRegister;
        }

        public void Process(IEvent eventToProcess)
        {
            var eventCommands = _eventCommandRegister.GetCommands(eventToProcess);

            eventCommands.ForEach(command => command.Execute(eventToProcess));
        }
    }
}