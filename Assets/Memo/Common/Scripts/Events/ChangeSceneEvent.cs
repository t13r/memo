namespace Memo.Common.Events
{
    public class ChangeSceneEvent : IEvent
    {
        public string SceneName { get; }

        private ChangeSceneEvent(string sceneName)
        {
            SceneName = sceneName;
        }

        public static ChangeSceneEvent Create(string sceneName)
        {
            return new ChangeSceneEvent(sceneName);
        }
    }
}