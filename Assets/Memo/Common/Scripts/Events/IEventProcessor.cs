namespace Memo.Common.Events
{
    public interface IEventProcessor
    {
        void Process(IEvent eventToProcess);
    }
}